import React from 'react'

const Contact = () => {
    return (

        <div className="container formulaire">
            <form>
                <div class="form-row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Prénom"></input>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Nom"></input>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Votre Texte</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <div class="col-auto my-1">
                    <button type="submit" class="btn btn-primary">Soumettre</button>
                </div>
                
            </form>

        </div>
    )
}

export default Contact
