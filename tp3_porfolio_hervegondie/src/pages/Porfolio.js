import React from 'react';
import PORFOLIODATA from '../documents/porfolioData.js';


const MonPorfolio = () => {
    const PorfolioDisplay = PORFOLIODATA.map((info) => {
        return (
            <main>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-2"></div>
                    <div className="col-8 alignement">
                        <div className="card text-center cadre bg-light "  >
                            <div className="card-body">
                                <h5 class="card-title">{info.nom}</h5>
                                <p>Cours: {info.cours}</p>
                                <a href={info.url} className="btn btn-primary">En savoir plus</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-2"></div>
                </div>
            </div>
            </main>
        )
    }
    )
    return (
        <div >
            {PorfolioDisplay}
        </div>
    )
}

export default MonPorfolio;


// import React, {useEffect, useState} from 'react';
// import CadreInfo from '../components/CadreInfo';



// const Porfolio = () => {
//     const[data, setData] =useState([]);

//     useEffect(() =>{
//         fetch('Porfolio.json', 
//         {
//       headers : { 
//         'Content-Type': 'application/json',
//         'Accept': 'application/json'
//        }
//     })
//     .then((response) =>setData(response.data));
   
//     }, []);
    
    
//     return (
//        <div>
//              {data.map((carteDescription) =>(
//                    <CadreInfo carteDescription={carteDescription} Key={carteDescription.nom}/>
//                ))} 
//        </div>
//     )
// }

// export default Porfolio
