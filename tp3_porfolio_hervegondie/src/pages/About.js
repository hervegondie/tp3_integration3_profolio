import React from 'react'

const About = () => {
    return (
        <main>
            <div className="description">
            <h2>Description</h2>
            <p>Je cumule plus de 5 ans d’expérience dans le domaine des technologies de l’information dont 2 années comme analyste fonctionnel. J’ai travaillé chez CGI Québec ou j’ai réalisé plusieurs mandats comme analyste fonctionnel en développement de systèmes informatiques et géomatiques. Actuellement je réalise un mandat avec la firme Levio comme analyste fonctionnel au ministère de l’Environnement et de la lutte contre les changements climatiques.</p>
            <p>J’ai une bonne capacité à comprendre les besoins des clients, à les analyser et les documenter dans les dossiers fonctionnels (P490) ainsi que les devis de tests fonctionnels (P750, P760). J’ai une bonne expérience en ce qui concerne la réalisation des essais fonctionnels dans le cadre du développement, de l’entretien ou de la maintenance des solutions applicatives. Je réalise actuellement les tests sur une application qui est en développement actuellement au MELCC</p>
            <p>J’ai aussi de l’expérience avec la documentation des cas des histoires d’utilisateurs (user story) dans Azure Devops, la rédaction des cas d’essai fonctionnel avec Test Plans.  Je suis aussi capable de réaliser les tâches qui me seront confiées dans le cadre de cet emploi d'analyste fonctionnel.</p>
            <p>Les technologies utilisées dans le cadre des mandats sont notamment le Javascript, le C#, le Python, Les outils de bases de données (SQL, PL/SQL). J’ai aussi la capacité de m’adapter à d’autres technologies nécessaires à la réalisation des mandats. J’ai une bonne maîtrise des méthodologies agiles à l’instar du scrum.</p>
            </div>
        </main>
    )
}

export default About
