import React from 'react';
import Typed from 'react-typed';

const Accueil = () => {
    return (
        <div>
            <div className = "home-container">
                <div className = "home-information">
                <h1>Developpement web et Design</h1>
                <Typed
                className = "text-auto"
                    strings={["Analyse et conception de site web", "Integration Web", "Programmation Web", "Design graphique et web", "Animation des pages web"]}
                    typeSpeed={40}
                    backSpeed={60}
                    loop
                />
                </div>
            </div>
            
        </div>
    )
}

export default Accueil
