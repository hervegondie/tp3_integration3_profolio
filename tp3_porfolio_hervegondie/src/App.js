import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Accueil from './pages/Accueil';
import Porfolio from "./pages/Porfolio"
import About from "./pages/About"
import Contact from "./pages/Contact"
import Navigation from './components/Navigation'
import Footer from './components/Footer'


const App = ()=> {
  return (
    <BrowserRouter>
    <Navigation/>
    <Switch>
      <Route path="/" exact component={Accueil} />
      <Route path="/porfolio" exact component={Porfolio} />
      <Route path="/About" exact component={About} />
      <Route path="/Contact" exact component={Contact} />
    </Switch>
    <Footer/>
  </BrowserRouter>
  );
}

export default App;
