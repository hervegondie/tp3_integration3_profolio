
const PORFOLIODATA = [
    {
        "id": 1,
        "nom": "Travail pratique 1 en Techniques d'intégration 1",
        "cours": "Techniques d'intégration 1",
        "url": "https://gitlab.com/hervegondie/tp1_integration_hervegondie"
    },

    {
        "id": 2,
        "nom": "Travail pratique 2 en Techniques d'intégration 1",
        "cours": "Techniques d'intégration 1",
        "url": "https://gitlab.com/hervegondie/tp2_integration_gondie_herve"
    },

    {
        "id": 3,
        "nom": "Travail pratique 3 en Techniques d'intégration 1",
        "cours": "Techniques d'intégration 1",
        "url": "https://gitlab.com/ourrimeriamworld/tp3-effix-two"
    },

    {
        "id": 4,
        "nom": "Travail pratique 4 en Techniques d'intégration 1",
        "cours": "Techniques d'intégration 1",
        "url": "https://gitlab.com/hervegondie/tp4_hervegondie_integration"
    },

    {
        "id": 5,
        "nom": "Travail pratique 2 en Techniques d'intégration 2",
        "cours": "Techniques d'intégration 2",
        "url": "https://gitlab.com/hervegondie/tp2_integration2_hervegondie"
    },

    {
        "id": 6,
        "nom": "Travail pratique 3 en Techniques d'animation",
        "cours": "Techniques d'animation",
        "url": "https://gitlab.com/bernier13julie/tp3_animation"
    },

    {
    "id": 7,
        "nom": "Travail pratique 3 en Techniques d'integration 2",
        "cours": "Techniques d'integration 2",
        "url": "https://gitlab.com/hervegondie/tp3_integration2quiz_hervegondie"
    },

    {
        "id": 8,
            "nom": "Projet de production 1",
            "cours": "Projet de production",
            "url": "https://gitlab.com/Giyom-webdev/team_gamma_project"
        },

        {
            "id": 8,
                "nom": "Projet de synthese",
                "cours": "Traitement des medias",
                "url": "https://gitlab.com/hervegondie/projet_synthese_traitement_media"
            },

]

    ;
export default PORFOLIODATA;