import React from "react";
import { NavLink } from 'react-router-dom';


const navigation = () => {
    return (
        <div className="navbar navbar-expand-lg navbar-light bg-dark">
            <div>
            <img src="./images/logo.png" alt="logo"></img>
            </div>
        <NavLink exact to="/" activeClassName="nav-active" className="nav-link menu">
            Accueil
        </NavLink>
        <NavLink exact to="/Porfolio" activeClassName="nav-active" className="nav-link menu">
           Porfolio
        </NavLink>
        <NavLink exact to="/About" activeClassName="nav-active" className="nav-link menu">
           À propos
        </NavLink>
        <NavLink exact to="/Contact" activeClassName="nav-active" className="nav-link menu">
           Nous contacter
        </NavLink>
    </div>
    )
}


export default navigation
