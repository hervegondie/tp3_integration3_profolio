import React from 'react';

const CadreInfo = (props) => {
    const carteDescription = props.carteDescription;
   
    return (
        <li >
            <p>{carteDescription.nom}</p>
        </li>
    )
}

export default CadreInfo;